//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/clientes/:idcliente', function(req, res) {
  res.send("Aqui tiene al cliente N. " + req.params.idcliente);
});

app.get('/clientes/:idcliente/:nombre', function(req, res) {
  res.send("Aqui tiene el cliente N. " + req.params.idcliente + " y su nombre es " + req.params.nombre);
})

var movimientosJSON = require('./movimientosv1.json');
app.get('/v1/movimientos', function(req, res) {
  res.send(movimientosJSON);
});

app.post('/', function(req, res) {
  res.send("Hemos recibido su preticion post")
});

app.put('/', function(req, res) {
  res.send("Hemos recibido su preticion put cambiada")
});

app.delete('/', function(req, res) {
  res.send("Hemos recibido su preticion delete")
});
